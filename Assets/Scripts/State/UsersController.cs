﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace UsersList
{
    public class UserInfo
    {
        public long   UID       = 0;
        public string FirstName = string.Empty;
        public string AvatarURL = string.Empty;

        public UserInfo(JSONObject obj)
        {
            obj.GetField(out UID      , "uid"       , 0);
            obj.GetField(out FirstName, "first_name", string.Empty);
            obj.GetField(out AvatarURL, "photo_200" , string.Empty);
        }
    }

    public class UsersController
    {
        public static SortedDictionary<long, UserInfo> LoadUsers()
        {
            var users = new SortedDictionary<long, UserInfo>();
            var path = "Configs/users.json";
            var text_asset = Resources.Load<TextAsset>(path);
            if (text_asset && !string.IsNullOrEmpty(text_asset.text))
            {
                var json_obj = JSONObject.Create(text_asset.text);
                if (json_obj)
                {
                    foreach (var obj in json_obj.list)
                    {
                        long uid;
                        if (obj.GetField(out uid, "uid", 0))
                        {
                            var info = new UserInfo(obj);
                            if (info != null)
                            {
                                users.Add(uid, info);
                            }
                        }
                    }
                }
            }
            return users;
        }
    }
}