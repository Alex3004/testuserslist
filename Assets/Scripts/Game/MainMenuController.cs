﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace UsersList
{
    public class MainMenuController : MonoBehaviour
    {
        public Button ButtonPlay = null;
        public Button ButtonQuit = null;

        void Start()
        {
            if (ButtonPlay)
            {
                ButtonPlay.onClick.RemoveAllListeners();
                ButtonPlay.onClick.AddListener(() =>
                {
                    SceneManager.LoadScene("Game", LoadSceneMode.Single);
                });
            }

            if (ButtonQuit)
            {
                ButtonQuit.onClick.RemoveAllListeners();
                ButtonQuit.onClick.AddListener(() =>
                {
                    Application.Quit();
                });
            }
        }
    }
}