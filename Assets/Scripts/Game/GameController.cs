﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

namespace UsersList
{
    public class GameController : MonoBehaviour
    {
        public Button     ButtonRun  = null;
        public ScrollRect Scroll     = null;
        public GameObject UserPrefab = null;

        public float Speed = 0.25f;

        void Start()
        {
            if (ButtonRun)
            {
                ButtonRun.onClick.RemoveAllListeners();
                ButtonRun.onClick.AddListener(() =>
                {
                    if (Scroll.verticalNormalizedPosition > 0)
                    {
                        var childs_count = Scroll.content.childCount;
                        DOTween.To(() => Scroll.verticalNormalizedPosition,
                            x => Scroll.verticalNormalizedPosition = x, 0, childs_count / Speed).SetEase(Ease.Linear);
                    }
                });
            }

            var users_info = UsersController.LoadUsers();
            foreach (var user_info in users_info.Values)
            {
                var user_go = Instantiate<GameObject>(UserPrefab);
                var user = user_go.GetComponent<User>();
                {
                    if (user)
                    {
                        user.PreSetupUser(user_info);
                        user_go.transform.SetParent(Scroll.content, false);
                    }
                }
            }
        }
    }
}
