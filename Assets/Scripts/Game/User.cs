﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace UsersList
{
    public class User : MonoBehaviour
    {
        public Text  UIDText       = null;
        public Text  FirstNameText = null;
        public Image AvatarImage   = null;

        private long _uid;

        public void PreSetupUser(UserInfo info)
        {
            _uid = info.UID;
            if (UIDText)
            {
                UIDText.text = info.UID.ToString();
            }

            if (FirstNameText)
            {
                FirstNameText.text = info.FirstName;
            }

            var image_loader = ImageLoader.Instance;
            var sprite_info = image_loader.GetOrCreateSpriteInfo(info.UID, info.AvatarURL);
            if (sprite_info.Sprite == null)
            {
                sprite_info.SpriteLoaded += SetupUserImage;
            }
            else
            {
                SetupUserImage(sprite_info.Sprite, info.UID);
            }
        }

        public void SetupUserImage(Sprite sprite, long uid)
        {
            if (_uid == uid)
            {
                if (AvatarImage)
                {
                    AvatarImage.sprite = sprite;
                }
            }
        }

        void OnDisable()
        {
            var loader = ImageLoader.Instance;
            var sprite_info = loader.GetSpriteInfo(_uid);
            if (sprite_info != null)
            {
                sprite_info.SpriteLoaded -= SetupUserImage;
            }
        }
   }
}