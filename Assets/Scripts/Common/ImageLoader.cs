﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class ImageLoader : MonoBehaviour {

    public static ImageLoader Instance { get; private set; }

    Dictionary<long, SpriteInfo> _sprites = new Dictionary<long, SpriteInfo>();
    bool _loading = false;

    void Awake()
    {
        Instance = this;
    }

    public class SpriteInfo
    {
        public WWW                    www          = null;
        public Sprite                 Sprite       = null;
        public Action<Sprite, long>   SpriteLoaded = null;

        public SpriteInfo(string url)
        {
            www = new WWW(url);
        }

        public void OnSpriteLoaded(Sprite sprite, long uid)
        {
            Sprite = sprite;
            if (SpriteLoaded != null && Sprite != null)
            {
                SpriteLoaded(Sprite, uid);
            }
        }
    }

    public SpriteInfo GetSpriteInfo(long uid)
    {
        SpriteInfo sprite_info;
        return _sprites.TryGetValue(uid, out sprite_info)
            ? sprite_info : null;
    }

    public SpriteInfo GetOrCreateSpriteInfo(long uid, string url)
    {
        SpriteInfo sprite_info;
        if (_sprites.TryGetValue(uid, out sprite_info))
        {
            return sprite_info;
        }
        _loading = true;
        var info = new SpriteInfo(url);
        _sprites.Add(uid, info);
        return info;
    }

    private void Update()
    {
        if (_loading)
        {
            _loading = false;
            foreach (var info in _sprites)
            {
                if (info.Value.www != null)
                {
                    if (!string.IsNullOrEmpty(info.Value.www.error))
                    {
                        info.Value.www = null;
                        return;
                    }
                    if (info.Value.www.isDone)
                    {
                        var texture = info.Value.www.texture;
                        if (texture)
                        {
                            var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                            info.Value.OnSpriteLoaded(sprite, info.Key);
                            info.Value.www = null;
                        }
                    }
                    else
                    {
                        _loading = true;
                    }
                }
            }
        }
    }
}
